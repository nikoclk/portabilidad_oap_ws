
package com.epcs.acreditacionoap.clientedocumentosacepta.to;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "institution",
    "extra",
    "session_id"
})
public class DocumentRequest {

    @JsonProperty("code")
    private List<String> code = new ArrayList<String>();
    @JsonProperty("institution")
    private String institution;
    @JsonProperty("extra")
    private String extra;
    @JsonProperty("session_id")
    private String sessionId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public DocumentRequest() {
    }

    public DocumentRequest(List<String> code, String institution, String extra, String sessionId) {
        this.code = code;
        this.institution = institution;
        this.extra = extra;
        this.sessionId = sessionId;
    }

    @JsonProperty("code")
    public List<String> getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(List<String> code) {
        this.code = code;
    }

    @JsonProperty("institution")
    public String getInstitution() {
        return institution;
    }

    @JsonProperty("institution")
    public void setInstitution(String institution) {
        this.institution = institution;
    }

    @JsonProperty("extra")
    public String getExtra() {
        return extra;
    }

    @JsonProperty("extra")
    public void setExtra(String extra) {
        this.extra = extra;
    }

    @JsonProperty("session_id")
    public String getSessionId() {
        return sessionId;
    }

    @JsonProperty("session_id")
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", code).append("institution", institution).append("extra", extra).append("sessionId", sessionId).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(institution).append(code).append(sessionId).append(additionalProperties).append(extra).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DocumentRequest) == false) {
            return false;
        }
        DocumentRequest rhs = ((DocumentRequest) other);
        return new EqualsBuilder().append(institution, rhs.institution).append(code, rhs.code).append(sessionId, rhs.sessionId).append(additionalProperties, rhs.additionalProperties).append(extra, rhs.extra).isEquals();
    }

}
