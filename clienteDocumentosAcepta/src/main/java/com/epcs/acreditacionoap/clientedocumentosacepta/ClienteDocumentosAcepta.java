package com.epcs.acreditacionoap.clientedocumentosacepta;


import com.epcs.acreditacionoap.clientedocumentosacepta.to.DocumentRequest;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.DocumentResponse;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.LoginRequest;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.LoginResponse;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.LogoutRequest;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.LogoutResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Cliente detail customer tde.
 */
public final class ClienteDocumentosAcepta {

    /**
     * Main.
     *
     * @param args the args
     * @throws Exception the exception
     */
    public static void main(String args[]) throws Exception {
        String urlDoc = "https://4.dec.cl/files/ENTEL/K28000007A1E27BDOC.pdf";
        Matcher matcher = Pattern.compile(".+/(\\w+)\\.pdf").matcher(urlDoc);
        String doc = "";
        if (matcher.find()) {
            doc = matcher.group(1);
        }
        LoginResponse res = login(new LoginRequest("ENTEL", "cheH8T"),
                "6dbce3f49fd3bbd6cea8181d310d5bfc35cceccf",
                "https://5cap.dec.cl/api/v1/auth/login");
//        obtenerDocumento(new DocumentRequest("ENTEL", "file", res.getSessionId(), doc),
//                "https://5cap.dec.cl/api/v1/documents",
//                "6dbce3f49fd3bbd6cea8181d310d5bfc35cceccf");
        logout(new LogoutRequest(res.getSessionId()), "6dbce3f49fd3bbd6cea8181d310d5bfc35cceccf",
                "https://5cap.dec.cl/api/v1/auth/logout");
    }

    /**
     * Login login response.
     *
     * @param request the request
     * @param apiKey  the api key
     * @param url     the url
     * @return the login response
     * @throws Exception the exception
     */
    public static LoginResponse login(final LoginRequest request, final String apiKey, final String url) throws Exception {
        LoginResponse result;
        ObjectMapper mapper = new ObjectMapper();
        String res;

        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(),
                new String[]{"TLSv1.2"}, null, NoopHostnameVerifier.INSTANCE);
        CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(
                sslsf).build();

        HttpPost clientMethod;
        URIBuilder uriB = new URIBuilder(url);
//        uriB.addParameter("rutCliente", rutClient).addParameter("rutEjecutivo", rutEjecutivo);

        clientMethod = new HttpPost(uriB.build());
        StringEntity ent = new StringEntity(mapper.writeValueAsString(request));
        clientMethod.setEntity(ent);
        clientMethod.setHeader("X-API-KEY", apiKey);
        clientMethod.setHeader("Content-Type", "application/json");

        CloseableHttpResponse response = httpclient.execute(clientMethod);

        if (response.getStatusLine().getStatusCode() != 200) {
            HttpEntity entity = response.getEntity();

            Scanner scanner = new Scanner(entity.getContent(), "UTF-8");
            System.out.println(scanner.useDelimiter("\\A").next());
            throw new Exception("Error Http code: " + response.getStatusLine().getStatusCode());
        } else {
            try {
                HttpEntity entity = response.getEntity();

                Scanner scanner = new Scanner(entity.getContent(), "UTF-8");
                res = scanner.useDelimiter("\\A").next();

                EntityUtils.consume(entity);
                result = mapper.readValue(res, LoginResponse.class);
            } finally {
                response.close();
            }
        }

        return result;
    }

    /**
     * Logout logout response.
     *
     * @param request the request
     * @param apiKey  the api key
     * @param url     the url
     * @return the logout response
     * @throws Exception the exception
     */
    public static LogoutResponse logout(final LogoutRequest request,
                                        final String apiKey, final String url) throws Exception {
        LogoutResponse result;
        ObjectMapper mapper = new ObjectMapper();
        String res;

        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(),
                new String[]{"TLSv1.2"}, null, NoopHostnameVerifier.INSTANCE);
        CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(
                sslsf).build();

        HttpPost clientMethod;
        URIBuilder uriB = new URIBuilder(url);
//        uriB.addParameter("rutCliente", rutClient).addParameter("rutEjecutivo", rutEjecutivo);

        clientMethod = new HttpPost(uriB.build());
        StringEntity ent = new StringEntity(mapper.writeValueAsString(request));
        clientMethod.setEntity(ent);
        clientMethod.setHeader("X-API-KEY", apiKey);
        clientMethod.setHeader("Content-Type", "application/json");

        CloseableHttpResponse response = httpclient.execute(clientMethod);

        if (response.getStatusLine().getStatusCode() != 200) {
            HttpEntity entity = response.getEntity();

            Scanner scanner = new Scanner(entity.getContent(), "UTF-8");
            System.out.println(scanner.useDelimiter("\\A").next());
            throw new Exception("Error Http code: " + response.getStatusLine().getStatusCode());
        } else {
            try {
                HttpEntity entity = response.getEntity();

                Scanner scanner = new Scanner(entity.getContent(), "UTF-8");
                res = scanner.useDelimiter("\\A").next();

                EntityUtils.consume(entity);
                result = mapper.readValue(res, LogoutResponse.class);
            } finally {
                response.close();
            }
        }

        return result;
    }

    public static DocumentResponse obtenerDocumento(final DocumentRequest request, final String url, final String apiKey) throws Exception {
        String res;
        ObjectMapper mapper = new ObjectMapper();
        DocumentResponse result;
        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(),
                new String[]{"TLSv1.2"}, null, NoopHostnameVerifier.INSTANCE);
        CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(
                sslsf).build();

        HttpPost clientMethod;
        URIBuilder uriB = new URIBuilder(url);
        StringEntity ent = new StringEntity(mapper.writeValueAsString(request));

        clientMethod = new HttpPost(uriB.build());
        clientMethod.setEntity(ent);
        clientMethod.setHeader("X-API-KEY", apiKey);
        clientMethod.setHeader("Content-Type", "application/json");

        CloseableHttpResponse response = httpclient.execute(clientMethod);

        if (response.getStatusLine().getStatusCode() != 200) {
            HttpEntity entity = response.getEntity();

            Scanner scanner = new Scanner(entity.getContent(), "UTF-8");
            System.out.println(scanner.useDelimiter("\\A").next());
            throw new Exception("Error Http code: " + response.getStatusLine().getStatusCode());
        } else {
            try {
                HttpEntity entity = response.getEntity();

                Scanner scanner = new Scanner(entity.getContent(), "UTF-8");
                res = scanner.useDelimiter("\\A").next();

                EntityUtils.consume(entity);
                result = mapper.readValue(res, DocumentResponse.class);
            } finally {
                response.close();
            }
        }

        return result;
    }
}
