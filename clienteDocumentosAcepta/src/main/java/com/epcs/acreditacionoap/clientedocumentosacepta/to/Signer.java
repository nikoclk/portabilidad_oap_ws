
package com.epcs.acreditacionoap.clientedocumentosacepta.to;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "role",
    "institution",
    "rut",
    "email",
    "type",
    "state",
    "order",
    "date",
    "audit",
    "notify",
    "who_signs"
})
public class Signer {

    @JsonProperty("name")
    private String name;
    @JsonProperty("role")
    private String role;
    @JsonProperty("institution")
    private String institution;
    @JsonProperty("rut")
    private String rut;
    @JsonProperty("email")
    private String email;
    @JsonProperty("type")
    private String type;
    @JsonProperty("state")
    private Integer state;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("date")
    private String date;
    @JsonProperty("audit")
    private String audit;
    @JsonProperty("notify")
    private Integer notify;
    @JsonProperty("who_signs")
    private Integer whoSigns;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("role")
    public String getRole() {
        return role;
    }

    @JsonProperty("role")
    public void setRole(String role) {
        this.role = role;
    }

    @JsonProperty("institution")
    public String getInstitution() {
        return institution;
    }

    @JsonProperty("institution")
    public void setInstitution(String institution) {
        this.institution = institution;
    }

    @JsonProperty("rut")
    public String getRut() {
        return rut;
    }

    @JsonProperty("rut")
    public void setRut(String rut) {
        this.rut = rut;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("state")
    public Integer getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(Integer state) {
        this.state = state;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("audit")
    public String getAudit() {
        return audit;
    }

    @JsonProperty("audit")
    public void setAudit(String audit) {
        this.audit = audit;
    }

    @JsonProperty("notify")
    public Integer getNotify() {
        return notify;
    }

    @JsonProperty("notify")
    public void setNotify(Integer notify) {
        this.notify = notify;
    }

    @JsonProperty("who_signs")
    public Integer getWhoSigns() {
        return whoSigns;
    }

    @JsonProperty("who_signs")
    public void setWhoSigns(Integer whoSigns) {
        this.whoSigns = whoSigns;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("role", role).append("institution", institution).append("rut", rut).append("email", email).append("type", type).append("state", state).append("order", order).append("date", date).append("audit", audit).append("notify", notify).append("whoSigns", whoSigns).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(date).append(role).append(type).append(notify).append(rut).append(institution).append(audit).append(name).append(whoSigns).append(state).append(additionalProperties).append(email).append(order).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Signer) == false) {
            return false;
        }
        Signer rhs = ((Signer) other);
        return new EqualsBuilder().append(date, rhs.date).append(role, rhs.role).append(type, rhs.type).append(notify, rhs.notify).append(rut, rhs.rut).append(institution, rhs.institution).append(audit, rhs.audit).append(name, rhs.name).append(whoSigns, rhs.whoSigns).append(state, rhs.state).append(additionalProperties, rhs.additionalProperties).append(email, rhs.email).append(order, rhs.order).isEquals();
    }

}
