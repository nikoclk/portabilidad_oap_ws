
package com.epcs.acreditacionoap.clientedocumentosacepta.to;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "code",
    "type_code",
    "country_code",
    "state",
    "date",
    "end_date",
    "can_sign",
    "can_sign_info",
    "security",
    "file_preview",
    "creator",
    "signers",
    "preview",
    "preview_mime",
    "file",
    "file_mime",
    "file_size",
    "md5",
    "thumbnail",
    "thumbnail_mime",
    "tags",
    "folders",
    "related",
    "comments",
    "qr"
})
public class Result {

    @JsonProperty("name")
    private String name;
    @JsonProperty("code")
    private String code;
    @JsonProperty("type_code")
    private String typeCode;
    @JsonProperty("country_code")
    private String countryCode;
    @JsonProperty("state")
    private Integer state;
    @JsonProperty("date")
    private String date;
    @JsonProperty("end_date")
    private String endDate;
    @JsonProperty("can_sign")
    private Integer canSign;
    @JsonProperty("can_sign_info")
    private List<CanSignInfo> canSignInfo = new ArrayList<CanSignInfo>();
    @JsonProperty("security")
    private Integer security;
    @JsonProperty("file_preview")
    private String filePreview;
    @JsonProperty("creator")
    private Creator creator;
    @JsonProperty("signers")
    private List<Signer> signers = new ArrayList<Signer>();
    @JsonProperty("preview")
    private String preview;
    @JsonProperty("preview_mime")
    private String previewMime;
    @JsonProperty("file")
    private String file;
    @JsonProperty("file_mime")
    private String fileMime;
    @JsonProperty("file_size")
    private Integer fileSize;
    @JsonProperty("md5")
    private String md5;
    @JsonProperty("thumbnail")
    private String thumbnail;
    @JsonProperty("thumbnail_mime")
    private String thumbnailMime;
    @JsonProperty("tags")
    private Tags tags;
    @JsonProperty("folders")
    private List<String> folders = new ArrayList<String>();
    @JsonProperty("related")
    private List<Related> related = new ArrayList<Related>();
    @JsonProperty("comments")
    private List<Comment> comments = new ArrayList<Comment>();
    @JsonProperty("qr")
    private String qr;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("type_code")
    public String getTypeCode() {
        return typeCode;
    }

    @JsonProperty("type_code")
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @JsonProperty("country_code")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("country_code")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonProperty("state")
    public Integer getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(Integer state) {
        this.state = state;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("end_date")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("end_date")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @JsonProperty("can_sign")
    public Integer getCanSign() {
        return canSign;
    }

    @JsonProperty("can_sign")
    public void setCanSign(Integer canSign) {
        this.canSign = canSign;
    }

    @JsonProperty("can_sign_info")
    public List<CanSignInfo> getCanSignInfo() {
        return canSignInfo;
    }

    @JsonProperty("can_sign_info")
    public void setCanSignInfo(List<CanSignInfo> canSignInfo) {
        this.canSignInfo = canSignInfo;
    }

    @JsonProperty("security")
    public Integer getSecurity() {
        return security;
    }

    @JsonProperty("security")
    public void setSecurity(Integer security) {
        this.security = security;
    }

    @JsonProperty("file_preview")
    public String getFilePreview() {
        return filePreview;
    }

    @JsonProperty("file_preview")
    public void setFilePreview(String filePreview) {
        this.filePreview = filePreview;
    }

    @JsonProperty("creator")
    public Creator getCreator() {
        return creator;
    }

    @JsonProperty("creator")
    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    @JsonProperty("signers")
    public List<Signer> getSigners() {
        return signers;
    }

    @JsonProperty("signers")
    public void setSigners(List<Signer> signers) {
        this.signers = signers;
    }

    @JsonProperty("preview")
    public String getPreview() {
        return preview;
    }

    @JsonProperty("preview")
    public void setPreview(String preview) {
        this.preview = preview;
    }

    @JsonProperty("preview_mime")
    public String getPreviewMime() {
        return previewMime;
    }

    @JsonProperty("preview_mime")
    public void setPreviewMime(String previewMime) {
        this.previewMime = previewMime;
    }

    @JsonProperty("file")
    public String getFile() {
        return file;
    }

    @JsonProperty("file")
    public void setFile(String file) {
        this.file = file;
    }

    @JsonProperty("file_mime")
    public String getFileMime() {
        return fileMime;
    }

    @JsonProperty("file_mime")
    public void setFileMime(String fileMime) {
        this.fileMime = fileMime;
    }

    @JsonProperty("file_size")
    public Integer getFileSize() {
        return fileSize;
    }

    @JsonProperty("file_size")
    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    @JsonProperty("md5")
    public String getMd5() {
        return md5;
    }

    @JsonProperty("md5")
    public void setMd5(String md5) {
        this.md5 = md5;
    }

    @JsonProperty("thumbnail")
    public String getThumbnail() {
        return thumbnail;
    }

    @JsonProperty("thumbnail")
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @JsonProperty("thumbnail_mime")
    public String getThumbnailMime() {
        return thumbnailMime;
    }

    @JsonProperty("thumbnail_mime")
    public void setThumbnailMime(String thumbnailMime) {
        this.thumbnailMime = thumbnailMime;
    }

    @JsonProperty("tags")
    public Tags getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(Tags tags) {
        this.tags = tags;
    }

    @JsonProperty("folders")
    public List<String> getFolders() {
        return folders;
    }

    @JsonProperty("folders")
    public void setFolders(List<String> folders) {
        this.folders = folders;
    }

    @JsonProperty("related")
    public List<Related> getRelated() {
        return related;
    }

    @JsonProperty("related")
    public void setRelated(List<Related> related) {
        this.related = related;
    }

    @JsonProperty("comments")
    public List<Comment> getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @JsonProperty("qr")
    public String getQr() {
        return qr;
    }

    @JsonProperty("qr")
    public void setQr(String qr) {
        this.qr = qr;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("code", code).append("typeCode", typeCode).append("countryCode", countryCode).append("state", state).append("date", date).append("endDate", endDate).append("canSign", canSign).append("canSignInfo", canSignInfo).append("security", security).append("filePreview", filePreview).append("creator", creator).append("signers", signers).append("preview", preview).append("previewMime", previewMime).append("file", file).append("fileMime", fileMime).append("fileSize", fileSize).append("md5", md5).append("thumbnail", thumbnail).append("thumbnailMime", thumbnailMime).append("tags", tags).append("folders", folders).append("related", related).append("comments", comments).append("qr", qr).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(date).append(preview).append(code).append(folders).append(endDate).append(thumbnailMime).append(security).append(file).append(related).append(countryCode).append(filePreview).append(previewMime).append(state).append(qr).append(creator).append(thumbnail).append(comments).append(canSign).append(canSignInfo).append(typeCode).append(tags).append(signers).append(fileMime).append(fileSize).append(name).append(additionalProperties).append(md5).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Result) == false) {
            return false;
        }
        Result rhs = ((Result) other);
        return new EqualsBuilder().append(date, rhs.date).append(preview, rhs.preview).append(code, rhs.code).append(folders, rhs.folders).append(endDate, rhs.endDate).append(thumbnailMime, rhs.thumbnailMime).append(security, rhs.security).append(file, rhs.file).append(related, rhs.related).append(countryCode, rhs.countryCode).append(filePreview, rhs.filePreview).append(previewMime, rhs.previewMime).append(state, rhs.state).append(qr, rhs.qr).append(creator, rhs.creator).append(thumbnail, rhs.thumbnail).append(comments, rhs.comments).append(canSign, rhs.canSign).append(canSignInfo, rhs.canSignInfo).append(typeCode, rhs.typeCode).append(tags, rhs.tags).append(signers, rhs.signers).append(fileMime, rhs.fileMime).append(fileSize, rhs.fileSize).append(name, rhs.name).append(additionalProperties, rhs.additionalProperties).append(md5, rhs.md5).isEquals();
    }

}
