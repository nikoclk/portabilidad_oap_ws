
package com.epcs.acreditacionoap.clientedocumentosacepta.to;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "user_name",
    "user_pin"
})
public class LoginRequest {

    @JsonProperty("user_name")
    private String userName;
    @JsonProperty("user_pin")
    private String userPin;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public LoginRequest() {
    }

    public LoginRequest(String userName, String userPin) {
        this.userName = userName;
        this.userPin = userPin;
    }

    @JsonProperty("user_name")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("user_name")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("user_pin")
    public String getUserPin() {
        return userPin;
    }

    @JsonProperty("user_pin")
    public void setUserPin(String userPin) {
        this.userPin = userPin;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userName", userName).append("userPin", userPin).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(userPin).append(additionalProperties).append(userName).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LoginRequest) == false) {
            return false;
        }
        LoginRequest rhs = ((LoginRequest) other);
        return new EqualsBuilder().append(userPin, rhs.userPin).append(additionalProperties, rhs.additionalProperties).append(userName, rhs.userName).isEquals();
    }

}
