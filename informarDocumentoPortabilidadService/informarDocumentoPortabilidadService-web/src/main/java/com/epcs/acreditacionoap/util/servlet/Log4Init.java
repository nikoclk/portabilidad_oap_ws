package com.epcs.acreditacionoap.util.servlet;

import cl.angecom.comunes.ConfigParam;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Log4Init.
 */
public class Log4Init extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(Log4Init.class);
    private static final String SERVICIO = "informarDocumentoPortabilidadService";
    private static final String PATH = "user.dir";
    private static final String NOMBRE = "dirAppConfig";
    private static final String FORMATO = "%s-%s";
    /**
     * Inicializa la configuracion de Log4j.
     * @param config Configuracion del servlet.
     * @throws javax.servlet.ServletException Excepcion generica.
     */
    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init();
        cargaInitConfig(config);
        cargaLog4j(config);
    }

    /**
     * Carga la configuracion de LOG4J desde archivo properties.
     * @param config Configuracion web.
     */
    private void cargaLog4j(final ServletConfig config) {
        InputStream is = null;
        String serverPath = System.getProperty(PATH)
                .replaceAll("\\\\", "/");
        String nombreMetodo = "cargaLog4 ";
        try {
            String file = config.getServletContext()
                    .getInitParameter("log4jConfig");
            String dirApp = config.getServletContext()
                    .getInitParameter(NOMBRE);
            is = new BufferedInputStream(new FileInputStream(serverPath
                    + dirApp + file));
            Properties p = new Properties();
            p.load(is);
            PropertyConfigurator.configure(p);
        } catch (IOException ex) {
            LOG.debug(String.format(FORMATO
                    , SERVICIO, nombreMetodo), ex);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ex) {
                LOG.debug(String.format(FORMATO
                        , SERVICIO, nombreMetodo), ex);
            }
        }
    }
    /**
     * Carga la configuracion de inicial desde archivo properties.
     * @param config Configuracion web.
     */
    private void cargaInitConfig(ServletConfig config) {
        InputStream is = null;
        String nombreMetodo = "cargaInitConfig ";
        String serverPath = System.getProperty(PATH)
                .replaceAll("\\\\", "/");
        try {
            String file = config.getServletContext()
                    .getInitParameter("initConfig");
            String dirApp = config.getServletContext()
                    .getInitParameter(NOMBRE);
            is = new BufferedInputStream(new FileInputStream(serverPath
                    + dirApp + file));
            ConfigParam.load(is);
        } catch (IOException ex) {
            LOG.debug(String.format(FORMATO
                    , SERVICIO, nombreMetodo), ex);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ex) {
                LOG.debug(String.format(FORMATO
                        , SERVICIO, nombreMetodo), ex);
            }
        }
    }

}
