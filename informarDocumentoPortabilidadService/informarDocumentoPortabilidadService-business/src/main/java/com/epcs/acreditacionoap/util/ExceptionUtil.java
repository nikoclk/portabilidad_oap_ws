package com.epcs.acreditacionoap.util;

import cl.angecom.comunes.ConfigParam;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws
        .InformarDocumentoPortabilidadServiceFaultMessage;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws
        .fault.InformarDocumentoPortabilidadServiceFault;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;


/**
 * Created by jorge on 08-03-2017.
 */
public final class ExceptionUtil {


    /**
     * Metodo privado.
     */
    private ExceptionUtil() {
    }

    /**
     * Generar fault desconocido.
     *
     * @param e the e.
     * @throws InformarDocumentoPortabilidadServiceFaultMessage the consultar imei aseguradas por rut se fault message.
     */
    public static void generarFaultDesconocido(Exception e) throws InformarDocumentoPortabilidadServiceFaultMessage {
        InformarDocumentoPortabilidadServiceFault faultType = new InformarDocumentoPortabilidadServiceFault();
        faultType.setCodiError(MsgEnum.FAULT_SISTEMA_GENERAL.getCode());
        faultType.setDescError(String.format(ConfigParam.getProperties(MsgEnum.FAULT_SISTEMA_GENERAL.getMsg()),
                e));
        faultType.setFechError(getTimeStamp());
        throw new InformarDocumentoPortabilidadServiceFaultMessage(
                faultType.getDescError(), faultType, e);
    }

    /**
     * Generar fault.
     *
     * @param e the e.
     * @throws InformarDocumentoPortabilidadServiceFaultMessage the consultar imei aseguradas por rut se fault message.
     */
    public static void generarFault(MsgEnum e) throws InformarDocumentoPortabilidadServiceFaultMessage {
        generarFault(e, null);
    }

    /**
     * Generar fault.
     *
     * @param msgEnum       the e.
     * @param infoAdicional the info adicional.
     * @throws InformarDocumentoPortabilidadServiceFaultMessage the consultar imei aseguradas por rut se fault message.
     */
    public static void generarFault(MsgEnum msgEnum, String infoAdicional)
            throws InformarDocumentoPortabilidadServiceFaultMessage {
        InformarDocumentoPortabilidadServiceFault faultType = new InformarDocumentoPortabilidadServiceFault();
        faultType.setCodiError(msgEnum.getCode());
        if (infoAdicional != null) {
            faultType.setDescError(String.format(ConfigParam.getProperties(msgEnum.getMsg()), infoAdicional));
        } else {
            faultType.setDescError(ConfigParam.getProperties(msgEnum.getMsg()));
        }
        faultType.setFechError(getTimeStamp());
        throw new InformarDocumentoPortabilidadServiceFaultMessage(
                faultType.getDescError(),
                faultType);
    }

    /**
     * Generar fault requerido.
     *
     * @param campo the campo.
     * @throws InformarDocumentoPortabilidadServiceFaultMessage the consultar equipos asegurable se fault message.
     */
    public static void generarFaultRequerido(String campo)
            throws InformarDocumentoPortabilidadServiceFaultMessage {
        InformarDocumentoPortabilidadServiceFault faultType = new InformarDocumentoPortabilidadServiceFault();
        faultType.setCodiError(MsgEnum.FAULT_NEGOCIO_FALTA_CAMPO_OBLIGATORIO.getCode());
        faultType.setDescError(String.format(ConfigParam.getProperties(MsgEnum.FAULT_NEGOCIO_FALTA_CAMPO_OBLIGATORIO
                        .getMsg()), campo));
        faultType.setFechError(getTimeStamp());
        throw new InformarDocumentoPortabilidadServiceFaultMessage(
                faultType.getDescError(), faultType);
    }
    
    private static XMLGregorianCalendar getTimeStamp() {
        return new XMLGregorianCalendarImpl(new GregorianCalendar());
    }
}
