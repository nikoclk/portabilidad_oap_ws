package com.epcs.acreditacionoap.util;

import org.apache.log4j.Logger;

/**
 * The type Log util.
 */
public final class LogUtil {

    /**
     * The constant SERVICE_NAME_CAMEL.
     */
    public static final String SERVICE_NAME_CAMEL = "InformarDocumentoPortabilidadService";
    /**
     * The constant TRAZA_INICIA.
     */
    private static final String TRAZA_INICIA = "INICIO -- %s";
    /**
     * The constant TRAZA_INICIA_PARAM.
     */
    private static final String TRAZA_INICIA_PARAM = TRAZA_INICIA + " | Parametros: %s";
    /**
     * The constant TRAZA_FIN.
     */
    private static final String TRAZA_FIN = "FIN -- %s";
    /**
     * The constant TRAZA_FIN_RESULT.
     */
    private static final String TRAZA_FIN_RESULT = TRAZA_FIN + " | Resultado: %s";

    private LogUtil() {
    }

    /**
     * Log entrada.
     *
     * @param logger the logger.
     * @param metodo the metodo.
     */
    public static void logEntrada(Logger logger, String metodo) {
        logger.info(String.format(TRAZA_INICIA, metodo.toUpperCase()));
    }

    /**
     * Log entrada.
     *
     * @param logger  the logger.
     * @param metodo  the metodo.
     * @param valores the valores.
     */
    public static void logEntrada(Logger logger, String metodo, String valores) {
        logger.info(String.format(TRAZA_INICIA_PARAM, metodo.toUpperCase(), valores));
    }

    /**
     * Log entrada.
     *
     * @param logger  the logger.
     * @param metodo  the metodo.
     * @param nombres the nombres.
     * @param valores the valores.
     */
    public static void logEntrada(Logger logger, String metodo, String[] nombres, Object... valores) {
        StringBuilder resultado = new StringBuilder();
        for (int i = 0; i < valores.length; i++) {
            if (resultado.length() != 0) {
                resultado.append(", ");
            }
            resultado.append(nombres[i]).append(" = ").append(String.valueOf(valores[i]));
        }
        logEntrada(logger, metodo, resultado.toString());
    }

    /**
     * Log salida.
     *
     * @param logger the logger.
     * @param metodo the metodo.
     */
    public static void logSalida(Logger logger, String metodo) {
        logger.info(String.format(TRAZA_FIN, metodo.toUpperCase()));
    }

    /**
     * Log salida.
     *
     * @param logger    the logger.
     * @param metodo    the metodo.
     * @param resultado the resultado.
     */
    public static void logSalida(Logger logger, String metodo, Object resultado) {
        logger.info(String.format(TRAZA_FIN_RESULT, metodo.toUpperCase(), String.valueOf(resultado)));
    }
}
