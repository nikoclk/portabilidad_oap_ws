package com.epcs.acreditacionoap.business;

import cl.angecom.comunes.ConfigParam;
import cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync.Documento;
import cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync.ParamEnviaDocumentosAsync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync.SSGPEnviaDocumentosAsynClient;
import cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync.SSGPEnviaDocumentosAsyncException_Exception;
import com.epcs.acreditacionoap.clientedocumentosacepta.ClienteDocumentosAcepta;
import com.epcs.acreditacionoap.clientedocumentosacepta.TLSSocketConnectionFactory;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.ObtieneDocumentosEntradaType;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.ObtieneDocumentosSalidaType;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.clienteDocumentosADR;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.clienteDocumentosSAED;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.DocumentRequest;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.DocumentResponse;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.LoginRequest;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.LoginResponse;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.LogoutRequest;
import com.epcs.acreditacionoap.clientedocumentosacepta.to.Result;

import com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.ObtieneDocumentosSAEDEntradaType;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.ObtieneDocumentosSAEDSalidaType;

import com.epcs.acreditacionoap.util.ExceptionUtil;
import com.epcs.acreditacionoap.util.LogUtil;
import com.epcs.acreditacionoap.util.MsgEnum;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws
        .InformarDocumentoPortabilidadServiceFaultMessage;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws.request.DocumentosType;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws
        .request.InformarDocumentoPortabilidadServiceRequest;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws
        .response.InformarDocumentoPortabilidadServiceResponse;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ${nombreEJB}Business.
 */
public class InformarDocumentoPortabilidadServiceBusiness {

    private static final Logger LOGGER = Logger.getLogger(InformarDocumentoPortabilidadServiceBusiness.class);
    private static final String INFORMAROKCODIGO = ConfigParam.getProperties("informardocumentos.ok.codigo");
    private static final String INFORMAROKMSG = ConfigParam.getProperties("informardocumentos.ok.mensaje");

    /**
     * Instantiates a new Informar documento portabilidad service business.
     */
    public InformarDocumentoPortabilidadServiceBusiness() {
    }

    /**
     * Informar documento portabilidad informar documento portabilidad service response.
     *
     * @param request the request
     * @return the informar documento portabilidad service response
     */
    public InformarDocumentoPortabilidadServiceResponse informarDocumentoPortabilidad(
            InformarDocumentoPortabilidadServiceRequest request)
            throws InformarDocumentoPortabilidadServiceFaultMessage {
        String metodo = "informarDocumentoPortabilidad";
        LogUtil.logEntrada(LOGGER, "ID", request.getIdd());
        LogUtil.logEntrada(LOGGER, "IDPortabilidad", request.getIdPortabilidadOAP());
        LogUtil.logEntrada(LOGGER, "TipoDocumento", request.getDocumento().get(0).getTipoDocumento());
        LogUtil.logEntrada(LOGGER, "ANI", request.getDocumento().get(0).getAni());
        LogUtil.logEntrada(LOGGER, "URLDocumento", request.getDocumento().get(0).getUrlDocumento());
        LogUtil.logEntrada(LOGGER, "FLAG", request.getFlagADRSAED());
        LogUtil.logEntrada(LOGGER, "FolioDocumento", request.getFolioDocumentoADRSAED());
        LogUtil.logEntrada(LOGGER, "DocumentoFirmado", request.getDocumentoFirmado());
        InformarDocumentoPortabilidadServiceResponse response = null;
        int rTimeOut = 30000;
        response = new InformarDocumentoPortabilidadServiceResponse();
        String flagAccionSAED = ConfigParam.getProperties("informardocumentos.saed.flagaccion");
        String flagAccionADR = ConfigParam.getProperties("informardocumentos.adr.flagaccion");
        try {
            if ("N".equals(request.getDocumentoFirmado())) {
                if ("ADR".equals(request.getFlagADRSAED())) {
                    ObtieneDocumentosSalidaType respuestaADR = null;
                    ObtieneDocumentosEntradaType getDocADR = new ObtieneDocumentosEntradaType();
                    getDocADR.setFolioDocumento(request.getFolioDocumentoADRSAED());
                    getDocADR.setFlagAccion(flagAccionADR);
                    String url = ConfigParam.getProperties("informardocumentos.adr.url");
                    respuestaADR = clienteDocumentosADR.obtieneDocumentosRequest(url, rTimeOut, rTimeOut, getDocADR);
                    metodo="RespuestaADR";
                    LogUtil.logSalida(LOGGER, metodo + " [respuestaADR.getArchivo()]", respuestaADR.getArchivo());
                    LogUtil.logSalida(LOGGER, metodo + " [respuestaADR.getTipoDoc()]", respuestaADR.getTipoDoc());
                    enviarDocumentosADR(request, respuestaADR);
                    response.setCodigo(INFORMAROKCODIGO);
                    response.setMensaje(INFORMAROKMSG);
                } else if ("SAED".equals(request.getFlagADRSAED())) {
                    ObtieneDocumentosSAEDSalidaType respuestaSAED = null;
                    ObtieneDocumentosSAEDEntradaType getDocSAED = new ObtieneDocumentosSAEDEntradaType();
                    getDocSAED.setFolioDocumento(request.getFolioDocumentoADRSAED());
                    getDocSAED.setFlagAccion(flagAccionSAED);
                    String url = ConfigParam.getProperties("informardocumentos.saed.url");
                    respuestaSAED = clienteDocumentosSAED.obtieneDocumentosRequest(url, rTimeOut, rTimeOut, getDocSAED);
                    metodo="RespuestaSAED";
                    LogUtil.logSalida(LOGGER, metodo, respuestaSAED.toString());
                    enviarDocumentosSAED(request, respuestaSAED);
                    response.setCodigo(INFORMAROKCODIGO);
                    response.setMensaje(INFORMAROKMSG);
                }
            } else {
                if ("ADR".equals(request.getFlagADRSAED())) {
                    ObtieneDocumentosSalidaType respuestaADR = null;
                    ObtieneDocumentosEntradaType getDocADR = new ObtieneDocumentosEntradaType();
                    getDocADR.setFolioDocumento(request.getFolioDocumentoADRSAED());
                    getDocADR.setFlagAccion(flagAccionADR);
                    String url = ConfigParam.getProperties("informardocumentos.adr.url");
                    respuestaADR = clienteDocumentosADR.obtieneDocumentosRequest(url, rTimeOut, rTimeOut, getDocADR);
                    metodo="RespuestaADR";
                    LogUtil.logSalida(LOGGER, metodo + " [respuestaADR.getArchivo()]", respuestaADR.getArchivo());
                    LogUtil.logSalida(LOGGER, metodo + " [respuestaADR.getTipoDoc()]", respuestaADR.getTipoDoc());
                    enviarDocumentosADR(request, respuestaADR);
                    response.setCodigo(INFORMAROKCODIGO);
                    response.setMensaje(INFORMAROKMSG);
                } else if ("SAED".equals(request.getFlagADRSAED())) {
                    ObtieneDocumentosSAEDSalidaType respuestaSAED = null;
                    ObtieneDocumentosSAEDEntradaType getDocSAED = new ObtieneDocumentosSAEDEntradaType();
                    getDocSAED.setFolioDocumento(request.getFolioDocumentoADRSAED());
                    getDocSAED.setFlagAccion(flagAccionSAED);
                    String url = ConfigParam.getProperties("informardocumentos.saed.url");
                    respuestaSAED = clienteDocumentosSAED.obtieneDocumentosRequest(url, rTimeOut, rTimeOut, getDocSAED);
                    metodo="RespuestaSAED";
                    LogUtil.logSalida(LOGGER, metodo + " [respuestaSAED.getArchivo()]", respuestaSAED.getArchivo());
                    LogUtil.logSalida(LOGGER, metodo + " [respuestaSAED.getTipoDoc()]", respuestaSAED.getTipoDoc());
                    enviarDocumentosSAED(request, respuestaSAED);
                    response.setCodigo(INFORMAROKCODIGO);
                    response.setMensaje(INFORMAROKMSG);
                } else {
                    String url = ConfigParam.getProperties("informardocumentos.aceptadec4.url");
                    String xmlRequest = generaXML(request);
                    String textRsp = invocar(url, xmlRequest);
                    String [] codError = textRsp.split("<Err xsi:type=\"xsd:int\">");
                    String [] codErrorFormat = codError[1].split("</Err>");

                    if (!"".equals(textRsp) && "0".equals(codErrorFormat[0])) {
                        String [] respuesta = textRsp.split("<Archivo>");
                        String [] respuestaFormat = respuesta[1].split("</Archivo>");
                        enviarDocumentosAceptaDec4(request, respuestaFormat[0]);
                        response.setCodigo(INFORMAROKCODIGO);
                        response.setMensaje(INFORMAROKMSG);
                    } else {
                        List<String> codDocumentosDec5 = new ArrayList<String>();
                        Map<String, DocumentosType> codUrl = new HashMap<String, DocumentosType>();
                        Matcher matcher = Pattern.compile(".+/(\\w+)\\.pdf").matcher("");
                        for (DocumentosType documentosType : request.getDocumento()) {
                            matcher.reset(documentosType.getUrlDocumento());
                            if (matcher.find()) {
                                String cod = matcher.group(1);
                                codDocumentosDec5.add(cod);
                                codUrl.put(cod, documentosType);
                            }
                        }
                        DocumentResponse documentResponse = procesarAcepta(codDocumentosDec5);
                        if (documentResponse.getResult().size() > 0) {
                            enviarDocumentos(request, codUrl, documentResponse);
                            response.setCodigo(INFORMAROKCODIGO);
                            response.setMensaje(INFORMAROKMSG);
                        } else {
                            response.setCodigo(ConfigParam.getProperties("generico.codigo.fault.negocio.404"));
                            response.setMensaje(ConfigParam.getProperties("generico.mensaje.fault.negocio.404"));
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error", e);
            response.setCodigo(ConfigParam.getProperties("generico.codigo.fault.negocio.404"));
            response.setMensaje(ConfigParam.getProperties("generico.mensaje.fault.negocio.404"));
            LogUtil.logSalida(LOGGER, metodo, response);
            return response;
        }
        LogUtil.logSalida(LOGGER, metodo, response.toString());
        return response;
    }

    private void enviarDocumentos(InformarDocumentoPortabilidadServiceRequest request,
                                  Map<String, DocumentosType> codUrl, DocumentResponse documentResponse)
            throws MalformedURLException, SSGPEnviaDocumentosAsyncException_Exception {
        ParamEnviaDocumentosAsync requestDoc = new ParamEnviaDocumentosAsync();
        requestDoc.setIdd(request.getIdd());
        requestDoc.setIdPortabilidadOAP(request.getIdPortabilidadOAP());
        for (Result result : documentResponse.getResult()) {
            Documento document = new Documento();
            document.setANI(codUrl.get(result.getCode()).getAni());
            document.setDocumentoDigitalizado(result.getFile().getBytes());
            document.setUrl(codUrl.get(result.getCode()).getUrlDocumento());
            document.setTipoDocumento(codUrl.get(result.getCode()).getTipoDocumento());
            requestDoc.getListaDocumentos().add(document);
        }
        final String urlEnviaDoc = ConfigParam.getProperties("informardocumentos.enviadoc.url");
        final int readTimeout = Integer.parseInt(
                ConfigParam.getProperties("informardocumentos.enviadoc.readtimeout"));
        final int connTimeout = Integer.parseInt(
                ConfigParam.getProperties("informardocumentos.enviadoc.conntimeout"));
        SSGPEnviaDocumentosAsynClient.enviarDocumentosAsync(requestDoc, urlEnviaDoc, connTimeout, readTimeout);
    }

    private DocumentResponse procesarAcepta(List<String> codDocumentosDec5)
            throws InformarDocumentoPortabilidadServiceFaultMessage {
        DocumentResponse documentResponse = null;
        try {
            final String apiKey = ConfigParam.getProperties("informardocumentos.acepta.apikey");
            final String urlLogin = ConfigParam.getProperties("informardocumentos.acepta.urllogin");
            final String urlGetDoc = ConfigParam.getProperties("informardocumentos.acepta.urlgetdoc");
            final String urlLogout = ConfigParam.getProperties("informardocumentos.acepta.urllogout");
            final String institution = ConfigParam.getProperties("informardocumentos.acepta.institution");
            LoginResponse loginResponse = ClienteDocumentosAcepta.login(
                    new LoginRequest(ConfigParam.getProperties("informardocumentos.acepta.username"),
                            ConfigParam.getProperties("informardocumentos.acepta.userpin")), apiKey, urlLogin);
            documentResponse = ClienteDocumentosAcepta.obtenerDocumento(
                    new DocumentRequest(codDocumentosDec5, institution, "file",
                            loginResponse.getSessionId()), urlGetDoc, apiKey);
            ClienteDocumentosAcepta.logout(new LogoutRequest(loginResponse.getSessionId()), apiKey, urlLogout);
        } catch (Exception e) {
            LOGGER.error("Error al invocar servicios de acepta", e);
            ExceptionUtil.generarFault(MsgEnum.FAULT_SISTEMA_WS_FUERA_SERVICIO);
        }
        return documentResponse;
    }

    private void enviarDocumentosAceptaDec4(InformarDocumentoPortabilidadServiceRequest request, String respuesta)
            throws MalformedURLException, SSGPEnviaDocumentosAsyncException_Exception {
        ParamEnviaDocumentosAsync requestDoc = new ParamEnviaDocumentosAsync();
        requestDoc.setIdd(request.getIdd());
        requestDoc.setIdPortabilidadOAP(request.getIdPortabilidadOAP());
        byte[] byteArray = respuesta.getBytes();
        Documento document = new Documento();
        document.setANI(request.getDocumento().get(0).getAni());
        document.setDocumentoDigitalizado(byteArray);
        document.setUrl(request.getDocumento().get(0).getUrlDocumento());
        document.setTipoDocumento(request.getDocumento().get(0).getTipoDocumento());
        requestDoc.getListaDocumentos().add(document);

        final String urlEnviaDoc = ConfigParam.getProperties("informardocumentos.enviadoc.url");
        final int readTimeout = Integer.parseInt(
                ConfigParam.getProperties("informardocumentos.enviadoc.readtimeout"));
        final int connTimeout = Integer.parseInt(
                ConfigParam.getProperties("informardocumentos.enviadoc.conntimeout"));
        SSGPEnviaDocumentosAsynClient.enviarDocumentosAsync(requestDoc, urlEnviaDoc, connTimeout, readTimeout);
    }


    private void enviarDocumentosADR(InformarDocumentoPortabilidadServiceRequest request,
                                         ObtieneDocumentosSalidaType respuestaADR)
            throws MalformedURLException, SSGPEnviaDocumentosAsyncException_Exception {
        ParamEnviaDocumentosAsync requestDoc = new ParamEnviaDocumentosAsync();
        requestDoc.setIdd(request.getIdd());
        requestDoc.setIdPortabilidadOAP(request.getIdPortabilidadOAP());

        Documento document = new Documento();
        document.setANI(request.getDocumento().get(0).getAni());
        document.setDocumentoDigitalizado(respuestaADR.getArchivo());
        document.setUrl(request.getDocumento().get(0).getUrlDocumento());
        document.setTipoDocumento(respuestaADR.getTipoDoc());
        requestDoc.getListaDocumentos().add(document);

        final String urlEnviaDoc = ConfigParam.getProperties("informardocumentos.enviadoc.url");
        final int readTimeout = Integer.parseInt(
                ConfigParam.getProperties("informardocumentos.enviadoc.readtimeout"));
        final int connTimeout = Integer.parseInt(
                ConfigParam.getProperties("informardocumentos.enviadoc.conntimeout"));
        SSGPEnviaDocumentosAsynClient.enviarDocumentosAsync(requestDoc, urlEnviaDoc, connTimeout, readTimeout);
    }

    private void enviarDocumentosSAED(InformarDocumentoPortabilidadServiceRequest request,
                                      ObtieneDocumentosSAEDSalidaType respuestaSAED)
            throws MalformedURLException, SSGPEnviaDocumentosAsyncException_Exception {
        ParamEnviaDocumentosAsync requestDoc = new ParamEnviaDocumentosAsync();
        requestDoc.setIdd(request.getIdd());
        requestDoc.setIdPortabilidadOAP(request.getIdPortabilidadOAP());

        Documento document = new Documento();
        document.setANI(request.getDocumento().get(0).getAni());
        document.setDocumentoDigitalizado(respuestaSAED.getArchivo());
        document.setUrl(request.getDocumento().get(0).getUrlDocumento());
        document.setTipoDocumento(respuestaSAED.getTipoDoc());
        requestDoc.getListaDocumentos().add(document);

        final String urlEnviaDoc = ConfigParam.getProperties("informardocumentos.enviadoc.url");
        final int readTimeout = Integer.parseInt(
                ConfigParam.getProperties("informardocumentos.enviadoc.readtimeout"));
        final int connTimeout = Integer.parseInt(
                ConfigParam.getProperties("informardocumentos.enviadoc.conntimeout"));
        SSGPEnviaDocumentosAsynClient.enviarDocumentosAsync(requestDoc, urlEnviaDoc, connTimeout, readTimeout);
    }


    public static String invocar(final String url, final String requestText)
            throws NoSuchAlgorithmException, KeyStoreException, IOException {
        String res = "";
        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(),
                new String[]{"TLSv1.2"}, null, NoopHostnameVerifier.INSTANCE);
        CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(
                sslsf).build();

        HttpRequestBase clientMethod;

        Map<String, String> header = new HashMap<String, String>();

        header.put("Content-Type", "text/xml;charset=UTF-8\n");

            HttpPost post = new HttpPost(url);
            StringEntity request = new StringEntity(requestText);
            post.setEntity(request);

            clientMethod = post;
        for (Map.Entry<String, String> entry : header.entrySet()) {
            clientMethod.setHeader(entry.getKey(), entry.getValue());
        }

        CloseableHttpResponse response = httpclient.execute(clientMethod);
        LOGGER.debug("[invocar] Codigo Salida HTTTP: " + response.getStatusLine().getStatusCode());

        try {
            HttpEntity entity = response.getEntity();
            Scanner scanner = new Scanner(entity.getContent(), "UTF-8");
            res = scanner.useDelimiter("\\A").next();
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
        return res;
    }

    public static String generaXML(InformarDocumentoPortabilidadServiceRequest request) {
        String xmlRequest = "<soapenv:Envelope ";
        xmlRequest += "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
        xmlRequest += "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ";
        xmlRequest += "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:wsdocs4\">";
        xmlRequest += "<soapenv:Header/>";
        xmlRequest += "<soapenv:Body>";
        xmlRequest += "<urn:wsGetDoc soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
        xmlRequest += "<ReqGetDoc xsi:type=\"urn:CDocReq\">";
        xmlRequest += "<wsUsuario xsi:type=\"xsd:string\">";
        xmlRequest += ConfigParam.getProperties("informardocumentos.aceptadec4.user");
        xmlRequest += "</wsUsuario>";
        xmlRequest += "<wsClave xsi:type=\"xsd:string\">";
        xmlRequest += ConfigParam.getProperties("informardocumentos.aceptadec4.pass");
        xmlRequest += "</wsClave>";
        xmlRequest += "<CodPais xsi:type=\"xsd:string\">CL</CodPais>";
        xmlRequest += "<Institucion xsi:type=\"xsd:string\">ENTEL</Institucion>";
        xmlRequest += "<CodigoDoc xsi:type=\"xsd:string\">";
        xmlRequest += request.getFolioDocumentoADRSAED();
        xmlRequest += "</CodigoDoc>";
        xmlRequest += "<CodUrl xsi:type=\"xsd:string\">";
        xmlRequest += request.getDocumento().get(0).getUrlDocumento();
        xmlRequest += "</CodUrl>";
        xmlRequest += "<bCodUrl>" + "false" + "</bCodUrl>";
        xmlRequest += "<bSoloInfo>" + "false" + "</bSoloInfo>";
        xmlRequest += "<Interno>" + "0" + "</Interno>";
        xmlRequest += "</ReqGetDoc>";
        xmlRequest += "</urn:wsGetDoc>";
        xmlRequest += "</soapenv:Body>";
        xmlRequest += "</soapenv:Envelope>";

        return xmlRequest;
    }

}