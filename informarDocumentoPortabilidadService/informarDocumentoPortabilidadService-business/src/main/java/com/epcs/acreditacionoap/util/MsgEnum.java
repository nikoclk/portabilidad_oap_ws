package com.epcs.acreditacionoap.util;

/**
 * The enum Msg enum.
 */
public enum MsgEnum {

    /**
     * Exito msg enum.
     */
    EXITO("0", "generico.mensaje.exito.000"),
    /**
     * Exito sin data msg enum.
     */
    EXITO_SIN_DATA("1", "generico.mensaje.exito.001"),
    /**
     * Fault configuracion falta config properties msg enum.
     */
    FAULT_CONFIGURACION_FALTA_CONFIG_PROPERTIES("101", "generico.mensaje.fault.configuracion.101"),
    /**
     * Fault configuracion falta sql properties msg enum.
     */
    FAULT_CONFIGURACION_FALTA_SQL_PROPERTIES("102", "generico.mensaje.fault.configuracion.102"),
    /**
     * Fault configuracion lectura config msg enum.
     */
    FAULT_CONFIGURACION_LECTURA_CONFIG("103", "generico.mensaje.fault.configuracion.103"),
    /**
     * Fault configuracion config valor invalido msg enum.
     */
    FAULT_CONFIGURACION_CONFIG_VALOR_INVALIDO("104", "generico.mensaje.fault.configuracion.104"),
    /**
     * Fault configuracion jndi no configurado msg enum.
     */
    FAULT_CONFIGURACION_JNDI_NO_CONFIGURADO("105", "generico.mensaje.fault.configuracion.105"),
    /**
     * Fault sistema general msg enum.
     */
    FAULT_SISTEMA_GENERAL("201", "generico.mensaje.fault.sistema.201"),
    /**
     * Fault sistema ws fuera servicio msg enum.
     */
    FAULT_SISTEMA_WS_FUERA_SERVICIO("202", "generico.mensaje.fault.sistema.202"),
    /**
     * Fault basededatos conexion msg enum.
     */
    FAULT_BASEDEDATOS_CONEXION("301", "generico.mensaje.fault.basededatos.301"),
    /**
     * Fault basededatos ejecucion query msg enum.
     */
    FAULT_BASEDEDATOS_EJECUCION_QUERY("302", "generico.mensaje.fault.basededatos.302"),
    /**
     * Fault basededatos invocacion package msg enum.
     */
    FAULT_BASEDEDATOS_INVOCACION_PACKAGE("303", "generico.mensaje.fault.basededatos.303"),
    /**
     * Fault basededatos ejecucion procedimiento msg enum.
     */
    FAULT_BASEDEDATOS_EJECUCION_PROCEDIMIENTO("304", "generico.mensaje.fault.basededatos.304"),
    /**
     * Fault negocio falta campo obligatorio msg enum.
     */
    FAULT_NEGOCIO_FALTA_CAMPO_OBLIGATORIO("401", "generico.mensaje.fault.negocio.401"),
    /**
     * Fault negocio formato campo invalido msg enum.
     */
    FAULT_NEGOCIO_FORMATO_CAMPO_INVALIDO("402", "generico.mensaje.fault.negocio.402"),
    /**
     * Fault negocio numero negocio no encontrado msg enum.
     */
    FAULT_NEGOCIO_NUMERO_NEGOCIO_NO_ENCONTRADO("403", "generico.mensaje.fault.negocio.403"),
    /**
     * Fault negocio producto no encontrado msg enum.
     */
    FAULT_NEGOCIO_PRODUCTO_NO_ENCONTRADO("404", "generico.mensaje.fault.negocio.404"),
    /**
     * Fault negocio motivo invalido msg enum.
     */
    FAULT_NEGOCIO_MOTIVO_INVALIDO("405", "generico.mensaje.fault.negocio.405");

    private final String code;
    private final String msg;

    MsgEnum(String strCodigo, String strMessage) {
        code = strCodigo;
        msg = strMessage;
    }

    /**
     * Gets code.
     *
     * @return the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets msg.
     *
     * @return the msg.
     */
    public String getMsg() {
        return msg;
    }

}
