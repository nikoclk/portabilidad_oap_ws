package com.epcs.acreditacionoap.dao;

import cl.angecom.jdbc.DatabaseConnection;

/**
 * InformarDocumentoPortabilidadServiceDao.
 */
public class InformarDocumentoPortabilidadServiceDao extends DatabaseConnection {



    public InformarDocumentoPortabilidadServiceDao() {
        super("jdbc/informarDocumentoPortabilidadService", false);
    }
}
