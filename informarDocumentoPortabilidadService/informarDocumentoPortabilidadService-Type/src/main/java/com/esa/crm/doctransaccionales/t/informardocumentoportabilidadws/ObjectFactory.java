
package com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws.fault.InformarDocumentoPortabilidadServiceFault;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws.request.InformarDocumentoPortabilidadServiceRequest;
import com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws.response.InformarDocumentoPortabilidadServiceResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InformarDocumentoPortabilidadServiceResponseDocument_QNAME = new QName("http://www.esa.com/CRM/DocTransaccionales/T/informarDocumentoPortabilidadWS", "informarDocumentoPortabilidadServiceResponseDocument");
    private final static QName _InformarDocumentoPortabilidadServiceServiceFaultDocument_QNAME = new QName("http://www.esa.com/CRM/DocTransaccionales/T/informarDocumentoPortabilidadWS", "informarDocumentoPortabilidadServiceServiceFaultDocument");
    private final static QName _InformarDocumentoPortabilidadServiceRequestDocument_QNAME = new QName("http://www.esa.com/CRM/DocTransaccionales/T/informarDocumentoPortabilidadWS", "informarDocumentoPortabilidadServiceRequestDocument");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InformarDocumentoPortabilidadServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/CRM/DocTransaccionales/T/informarDocumentoPortabilidadWS", name = "informarDocumentoPortabilidadServiceResponseDocument")
    public JAXBElement<InformarDocumentoPortabilidadServiceResponse> createInformarDocumentoPortabilidadServiceResponseDocument(InformarDocumentoPortabilidadServiceResponse value) {
        return new JAXBElement<InformarDocumentoPortabilidadServiceResponse>(_InformarDocumentoPortabilidadServiceResponseDocument_QNAME, InformarDocumentoPortabilidadServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InformarDocumentoPortabilidadServiceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/CRM/DocTransaccionales/T/informarDocumentoPortabilidadWS", name = "informarDocumentoPortabilidadServiceServiceFaultDocument")
    public JAXBElement<InformarDocumentoPortabilidadServiceFault> createInformarDocumentoPortabilidadServiceServiceFaultDocument(InformarDocumentoPortabilidadServiceFault value) {
        return new JAXBElement<InformarDocumentoPortabilidadServiceFault>(_InformarDocumentoPortabilidadServiceServiceFaultDocument_QNAME, InformarDocumentoPortabilidadServiceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InformarDocumentoPortabilidadServiceRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/CRM/DocTransaccionales/T/informarDocumentoPortabilidadWS", name = "informarDocumentoPortabilidadServiceRequestDocument")
    public JAXBElement<InformarDocumentoPortabilidadServiceRequest> createInformarDocumentoPortabilidadServiceRequestDocument(InformarDocumentoPortabilidadServiceRequest value) {
        return new JAXBElement<InformarDocumentoPortabilidadServiceRequest>(_InformarDocumentoPortabilidadServiceRequestDocument_QNAME, InformarDocumentoPortabilidadServiceRequest.class, null, value);
    }

}
