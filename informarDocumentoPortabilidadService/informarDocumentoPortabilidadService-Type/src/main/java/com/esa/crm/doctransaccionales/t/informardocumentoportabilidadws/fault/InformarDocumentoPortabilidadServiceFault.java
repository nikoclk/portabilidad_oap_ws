
package com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws.fault;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for informarDocumentoPortabilidadServiceFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="informarDocumentoPortabilidadServiceFault">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechError" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "informarDocumentoPortabilidadServiceFault", propOrder = {
    "codiError",
    "descError",
    "fechError"
})
public class InformarDocumentoPortabilidadServiceFault
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String codiError;
    @XmlElement(required = true)
    protected String descError;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechError;

    /**
     * Gets the value of the codiError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiError() {
        return codiError;
    }

    /**
     * Sets the value of the codiError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiError(String value) {
        this.codiError = value;
    }

    /**
     * Gets the value of the descError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescError() {
        return descError;
    }

    /**
     * Sets the value of the descError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescError(String value) {
        this.descError = value;
    }

    /**
     * Gets the value of the fechError property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechError() {
        return fechError;
    }

    /**
     * Sets the value of the fechError property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechError(XMLGregorianCalendar value) {
        this.fechError = value;
    }

}
