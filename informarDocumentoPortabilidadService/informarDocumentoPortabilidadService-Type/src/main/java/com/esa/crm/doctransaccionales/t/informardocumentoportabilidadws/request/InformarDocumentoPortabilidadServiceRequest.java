
package com.esa.crm.doctransaccionales.t.informardocumentoportabilidadws.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for informarDocumentoPortabilidadServiceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="informarDocumentoPortabilidadServiceRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idPortabilidadOAP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="documento" type="{http://www.esa.com/CRM/DocTransaccionales/T/informarDocumentoPortabilidadWS/Request}DocumentosType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="flagADRSAED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="folioDocumentoADRSAED" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="documentoFirmado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "informarDocumentoPortabilidadServiceRequest", propOrder = {
    "idd",
    "idPortabilidadOAP",
    "documento",
    "flagADRSAED",
    "folioDocumentoADRSAED",
    "documentoFirmado"
})
public class InformarDocumentoPortabilidadServiceRequest
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String idd;
    @XmlElement(required = true)
    protected String idPortabilidadOAP;
    @XmlElement(nillable = true)
    protected List<DocumentosType> documento;
    @XmlElement(required = true)
    protected String flagADRSAED;
    @XmlElement(required = true)
    protected String folioDocumentoADRSAED;
    @XmlElement(required = true)
    protected String documentoFirmado;

    /**
     * Gets the value of the idd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdd() {
        return idd;
    }

    /**
     * Sets the value of the idd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdd(String value) {
        this.idd = value;
    }

    /**
     * Gets the value of the idPortabilidadOAP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPortabilidadOAP() {
        return idPortabilidadOAP;
    }

    /**
     * Sets the value of the idPortabilidadOAP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPortabilidadOAP(String value) {
        this.idPortabilidadOAP = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentosType }
     * 
     * 
     */
    public List<DocumentosType> getDocumento() {
        if (documento == null) {
            documento = new ArrayList<DocumentosType>();
        }
        return this.documento;
    }

    /**
     * Gets the value of the flagADRSAED property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagADRSAED() {
        return flagADRSAED;
    }

    /**
     * Sets the value of the flagADRSAED property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagADRSAED(String value) {
        this.flagADRSAED = value;
    }

    /**
     * Gets the value of the folioDocumentoADRSAED property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioDocumentoADRSAED() {
        return folioDocumentoADRSAED;
    }

    /**
     * Sets the value of the folioDocumentoADRSAED property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioDocumentoADRSAED(String value) {
        this.folioDocumentoADRSAED = value;
    }

    /**
     * Gets the value of the documentoFirmado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoFirmado() {
        return documentoFirmado;
    }

    /**
     * Sets the value of the documentoFirmado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoFirmado(String value) {
        this.documentoFirmado = value;
    }

}
