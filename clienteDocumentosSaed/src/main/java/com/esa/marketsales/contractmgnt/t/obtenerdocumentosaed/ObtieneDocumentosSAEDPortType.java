package com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.7.8
 * 2019-02-14T17:16:24.754-03:00
 * Generated source version: 2.7.8
 * 
 */
@WebService(targetNamespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED", name = "ObtieneDocumentosSAEDPortType")
@XmlSeeAlso({ObjectFactory.class, com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.type.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface ObtieneDocumentosSAEDPortType {

    @WebMethod(action = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED")
    @WebResult(name = "ObtieneDocumentosSAEDResponse", targetNamespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED/type", partName = "ObtieneDocumentosSAEDResponseParameter")
    public ObtieneDocumentosSAEDSalidaType obtenerDocumentosSAEDM(
        @WebParam(partName = "ObtieneDocumentosSAEDRequestParameter", name = "ObtieneDocumentosSAEDRequest", targetNamespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED/type")
        ObtieneDocumentosSAEDEntradaType obtieneDocumentosSAEDRequestParameter
    ) throws ObtieneDocumentosSAEDFaultMsg;
}
