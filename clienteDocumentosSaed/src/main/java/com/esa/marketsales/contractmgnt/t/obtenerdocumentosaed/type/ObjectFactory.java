
package com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.type;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.ErrorType;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.ObtieneDocumentosSAEDEntradaType;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.ObtieneDocumentosSAEDSalidaType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.type package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObtieneDocumentosSAEDRequest_QNAME = new QName("http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED/type", "ObtieneDocumentosSAEDRequest");
    private final static QName _ObtieneDocumentosSAEDFault_QNAME = new QName("http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED/type", "ObtieneDocumentosSAEDFault");
    private final static QName _ObtieneDocumentosSAEDResponse_QNAME = new QName("http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED/type", "ObtieneDocumentosSAEDResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed.type
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtieneDocumentosSAEDEntradaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED/type", name = "ObtieneDocumentosSAEDRequest")
    public JAXBElement<ObtieneDocumentosSAEDEntradaType> createObtieneDocumentosSAEDRequest(ObtieneDocumentosSAEDEntradaType value) {
        return new JAXBElement<ObtieneDocumentosSAEDEntradaType>(_ObtieneDocumentosSAEDRequest_QNAME, ObtieneDocumentosSAEDEntradaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErrorType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED/type", name = "ObtieneDocumentosSAEDFault")
    public JAXBElement<ErrorType> createObtieneDocumentosSAEDFault(ErrorType value) {
        return new JAXBElement<ErrorType>(_ObtieneDocumentosSAEDFault_QNAME, ErrorType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtieneDocumentosSAEDSalidaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoSAED/type", name = "ObtieneDocumentosSAEDResponse")
    public JAXBElement<ObtieneDocumentosSAEDSalidaType> createObtieneDocumentosSAEDResponse(ObtieneDocumentosSAEDSalidaType value) {
        return new JAXBElement<ObtieneDocumentosSAEDSalidaType>(_ObtieneDocumentosSAEDResponse_QNAME, ObtieneDocumentosSAEDSalidaType.class, null, value);
    }

}
