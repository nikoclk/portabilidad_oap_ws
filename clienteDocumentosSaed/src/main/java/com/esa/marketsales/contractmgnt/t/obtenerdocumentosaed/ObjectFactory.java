
package com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObtieneDocumentosSAEDEntradaType }
     * 
     */
    public ObtieneDocumentosSAEDEntradaType createObtieneDocumentosSAEDEntradaType() {
        return new ObtieneDocumentosSAEDEntradaType();
    }

    /**
     * Create an instance of {@link ErrorType }
     * 
     */
    public ErrorType createErrorType() {
        return new ErrorType();
    }

    /**
     * Create an instance of {@link ObtieneDocumentosSAEDSalidaType }
     * 
     */
    public ObtieneDocumentosSAEDSalidaType createObtieneDocumentosSAEDSalidaType() {
        return new ObtieneDocumentosSAEDSalidaType();
    }

}
