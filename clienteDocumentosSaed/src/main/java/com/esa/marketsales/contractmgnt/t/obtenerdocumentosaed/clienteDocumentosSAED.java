package com.esa.marketsales.contractmgnt.t.obtenerdocumentosaed;

import javax.xml.ws.BindingProvider;
import java.util.Map;

/**
 * Created by Nicolas Ortiz on 20/02/2019.
 */
public class clienteDocumentosSAED {

    /**
     * connect timeout for weblogic server.
     */
    private static final String WLS_CONNECT_TIMEOUT = "com.sun.xml.internal.ws.connect.timeout";
    /**
     * read timeout for weblogic server.
     */

    private static final String WLS_READ_TIMEOUT = "com.sun.xml.internal.ws.request.timeout";

    private static final String END_POINT = "javax.xml.ws.service.endpoint.address";

    private clienteDocumentosSAED() {
    }

    public static ObtieneDocumentosSAEDSalidaType obtieneDocumentosRequest(
            String url,
            int rTimeout,
            int cTimeout,
            ObtieneDocumentosSAEDEntradaType requestREQType
    ) throws ObtieneDocumentosSAEDFaultMsg {
        MARTPXObtenerDocumentoSAEDService ss = new MARTPXObtenerDocumentoSAEDService();
        ObtieneDocumentosSAEDPortType port = ss.getObtenerDocumentosSAEDM();

        Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();

        requestContext.put(WLS_READ_TIMEOUT, rTimeout);
        requestContext.put(WLS_CONNECT_TIMEOUT, cTimeout);
        requestContext.put(END_POINT, url);

        return port.obtenerDocumentosSAEDM(requestREQType);
    }
}