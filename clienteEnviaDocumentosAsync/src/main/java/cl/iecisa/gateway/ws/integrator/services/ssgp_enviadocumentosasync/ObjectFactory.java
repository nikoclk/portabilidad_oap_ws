
package cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EnviaDocumentosAsyncResponse_QNAME = new QName("http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/", "enviaDocumentosAsyncResponse");
    private final static QName _SSGPEnviaDocumentosAsyncException_QNAME = new QName("http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/", "SSGP_EnviaDocumentosAsyncException");
    private final static QName _EnviaDocumentosAsync_QNAME = new QName("http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/", "enviaDocumentosAsync");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RespuestaEnviaDocumentosAsync }
     * 
     */
    public RespuestaEnviaDocumentosAsync createRespuestaEnviaDocumentosAsync() {
        return new RespuestaEnviaDocumentosAsync();
    }

    /**
     * Create an instance of {@link EnviaDocumentosAsyncResponse }
     * 
     */
    public EnviaDocumentosAsyncResponse createEnviaDocumentosAsyncResponse() {
        return new EnviaDocumentosAsyncResponse();
    }

    /**
     * Create an instance of {@link SSGPEnviaDocumentosAsyncException }
     * 
     */
    public SSGPEnviaDocumentosAsyncException createSSGPEnviaDocumentosAsyncException() {
        return new SSGPEnviaDocumentosAsyncException();
    }

    /**
     * Create an instance of {@link Documento }
     * 
     */
    public Documento createDocumento() {
        return new Documento();
    }

    /**
     * Create an instance of {@link ParamEnviaDocumentosAsync }
     * 
     */
    public ParamEnviaDocumentosAsync createParamEnviaDocumentosAsync() {
        return new ParamEnviaDocumentosAsync();
    }

    /**
     * Create an instance of {@link EnviaDocumentosAsync }
     * 
     */
    public EnviaDocumentosAsync createEnviaDocumentosAsync() {
        return new EnviaDocumentosAsync();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviaDocumentosAsyncResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/", name = "enviaDocumentosAsyncResponse")
    public JAXBElement<EnviaDocumentosAsyncResponse> createEnviaDocumentosAsyncResponse(EnviaDocumentosAsyncResponse value) {
        return new JAXBElement<EnviaDocumentosAsyncResponse>(_EnviaDocumentosAsyncResponse_QNAME, EnviaDocumentosAsyncResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSGPEnviaDocumentosAsyncException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/", name = "SSGP_EnviaDocumentosAsyncException")
    public JAXBElement<SSGPEnviaDocumentosAsyncException> createSSGPEnviaDocumentosAsyncException(SSGPEnviaDocumentosAsyncException value) {
        return new JAXBElement<SSGPEnviaDocumentosAsyncException>(_SSGPEnviaDocumentosAsyncException_QNAME, SSGPEnviaDocumentosAsyncException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviaDocumentosAsync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/", name = "enviaDocumentosAsync")
    public JAXBElement<EnviaDocumentosAsync> createEnviaDocumentosAsync(EnviaDocumentosAsync value) {
        return new JAXBElement<EnviaDocumentosAsync>(_EnviaDocumentosAsync_QNAME, EnviaDocumentosAsync.class, null, value);
    }

}
