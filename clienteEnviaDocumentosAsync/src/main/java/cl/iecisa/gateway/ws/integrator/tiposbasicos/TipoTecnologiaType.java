
package cl.iecisa.gateway.ws.integrator.tiposbasicos;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoTecnologiaType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoTecnologiaType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="GSM"/>
 *     &lt;enumeration value="UMTS"/>
 *     &lt;enumeration value="GPRS"/>
 *     &lt;enumeration value="EDGE"/>
 *     &lt;enumeration value="LTE"/>
 *     &lt;enumeration value="IDEN"/>
 *     &lt;enumeration value="TDMA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TipoTecnologiaType", namespace = "http://integrator.ws.gateway.iecisa.cl/tiposBasicos")
@XmlEnum
public enum TipoTecnologiaType {

    GSM,
    UMTS,
    GPRS,
    EDGE,
    LTE,
    IDEN,
    TDMA;

    public String value() {
        return name();
    }

    public static TipoTecnologiaType fromValue(String v) {
        return valueOf(v);
    }

}
