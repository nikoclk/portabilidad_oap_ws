
package cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for respuestaEnviaDocumentosAsync complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="respuestaEnviaDocumentosAsync">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="errorDescription" type="{http://integrator.ws.gateway.iecisa.cl/tiposBasicos}CodeDescriptionType"/>
 *         &lt;element name="idSolicitudGW" type="{http://integrator.ws.gateway.iecisa.cl/tiposBasicos}RequestTransactionType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaEnviaDocumentosAsync", propOrder = {
    "errorCode",
    "errorDescription",
    "idSolicitudGW"
})
public class RespuestaEnviaDocumentosAsync {

    protected int errorCode;
    @XmlElement(required = true)
    protected String errorDescription;
    @XmlElement(required = true)
    protected String idSolicitudGW;

    /**
     * Gets the value of the errorCode property.
     * 
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     */
    public void setErrorCode(int value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * Sets the value of the errorDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorDescription(String value) {
        this.errorDescription = value;
    }

    /**
     * Gets the value of the idSolicitudGW property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSolicitudGW() {
        return idSolicitudGW;
    }

    /**
     * Sets the value of the idSolicitudGW property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSolicitudGW(String value) {
        this.idSolicitudGW = value;
    }

}
