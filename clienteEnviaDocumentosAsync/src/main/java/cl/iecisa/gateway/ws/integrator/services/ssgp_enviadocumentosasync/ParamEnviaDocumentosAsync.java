
package cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for paramEnviaDocumentosAsync complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="paramEnviaDocumentosAsync">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idPortabilidadOAP" type="{http://integrator.ws.gateway.iecisa.cl/tiposBasicos}OAPTrxIdType"/>
 *         &lt;element name="idd" type="{http://integrator.ws.gateway.iecisa.cl/tiposBasicos}IddType"/>
 *         &lt;element name="listaDocumentos" type="{http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/}documento" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paramEnviaDocumentosAsync", propOrder = {
    "idPortabilidadOAP",
    "idd",
    "listaDocumentos"
})
public class ParamEnviaDocumentosAsync {

    @XmlElement(required = true)
    protected String idPortabilidadOAP;
    @XmlElement(required = true)
    protected String idd;
    @XmlElement(required = true)
    protected List<Documento> listaDocumentos;

    /**
     * Gets the value of the idPortabilidadOAP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPortabilidadOAP() {
        return idPortabilidadOAP;
    }

    /**
     * Sets the value of the idPortabilidadOAP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPortabilidadOAP(String value) {
        this.idPortabilidadOAP = value;
    }

    /**
     * Gets the value of the idd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdd() {
        return idd;
    }

    /**
     * Sets the value of the idd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdd(String value) {
        this.idd = value;
    }

    /**
     * Gets the value of the listaDocumentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaDocumentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaDocumentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Documento }
     * 
     * 
     */
    public List<Documento> getListaDocumentos() {
        if (listaDocumentos == null) {
            listaDocumentos = new ArrayList<Documento>();
        }
        return this.listaDocumentos;
    }

}
