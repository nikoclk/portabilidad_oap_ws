
package cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enviaDocumentosAsync complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enviaDocumentosAsync">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paramEnviaDocumentosAsync" type="{http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/}paramEnviaDocumentosAsync" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enviaDocumentosAsync", propOrder = {
    "paramEnviaDocumentosAsync"
})
public class EnviaDocumentosAsync {

    protected ParamEnviaDocumentosAsync paramEnviaDocumentosAsync;

    /**
     * Gets the value of the paramEnviaDocumentosAsync property.
     * 
     * @return
     *     possible object is
     *     {@link ParamEnviaDocumentosAsync }
     *     
     */
    public ParamEnviaDocumentosAsync getParamEnviaDocumentosAsync() {
        return paramEnviaDocumentosAsync;
    }

    /**
     * Sets the value of the paramEnviaDocumentosAsync property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParamEnviaDocumentosAsync }
     *     
     */
    public void setParamEnviaDocumentosAsync(ParamEnviaDocumentosAsync value) {
        this.paramEnviaDocumentosAsync = value;
    }

}
