
package cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for documento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="documento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANI" type="{http://integrator.ws.gateway.iecisa.cl/tiposBasicos}TNDataType"/>
 *         &lt;element name="documentoDigitalizado" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documento", propOrder = {
    "ani",
    "documentoDigitalizado",
    "url",
    "tipoDocumento"
})
public class Documento {

    @XmlElement(name = "ANI", required = true)
    protected String ani;
    @XmlElement(required = true)
    protected byte[] documentoDigitalizado;
    @XmlElement(required = true)
    protected String url;
    @XmlElement(required = true)
    protected String tipoDocumento;

    /**
     * Gets the value of the ani property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getANI() {
        return ani;
    }

    /**
     * Sets the value of the ani property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setANI(String value) {
        this.ani = value;
    }

    /**
     * Gets the value of the documentoDigitalizado property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDocumentoDigitalizado() {
        return documentoDigitalizado;
    }

    /**
     * Sets the value of the documentoDigitalizado property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDocumentoDigitalizado(byte[] value) {
        this.documentoDigitalizado = ((byte[]) value);
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

}
