
package cl.iecisa.gateway.ws.integrator.services.ssgp_enviadocumentosasync;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enviaDocumentosAsyncResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enviaDocumentosAsyncResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enviaDocumentosAsyncResponse" type="{http://SSGP_EnviaDocumentosAsync.services.integrator.ws.gateway.iecisa.cl/}respuestaEnviaDocumentosAsync" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enviaDocumentosAsyncResponse", propOrder = {
    "enviaDocumentosAsyncResponse"
})
public class EnviaDocumentosAsyncResponse {

    protected RespuestaEnviaDocumentosAsync enviaDocumentosAsyncResponse;

    /**
     * Gets the value of the enviaDocumentosAsyncResponse property.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaEnviaDocumentosAsync }
     *     
     */
    public RespuestaEnviaDocumentosAsync getEnviaDocumentosAsyncResponse() {
        return enviaDocumentosAsyncResponse;
    }

    /**
     * Sets the value of the enviaDocumentosAsyncResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaEnviaDocumentosAsync }
     *     
     */
    public void setEnviaDocumentosAsyncResponse(RespuestaEnviaDocumentosAsync value) {
        this.enviaDocumentosAsyncResponse = value;
    }

}
