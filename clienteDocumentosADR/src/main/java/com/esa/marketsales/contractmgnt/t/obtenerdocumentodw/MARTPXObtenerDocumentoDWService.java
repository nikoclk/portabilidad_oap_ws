package com.esa.marketsales.contractmgnt.t.obtenerdocumentodw;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.7.8
 * 2019-02-15T11:30:52.353-03:00
 * Generated source version: 2.7.8
 * 
 */
@WebServiceClient(name = "MAR_T_PX_ObtenerDocumentoDWService", 
                  wsdlLocation = "/META-INF/wsdl/obtenerDocumentosADR.wsdl",
                  targetNamespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW") 
public class MARTPXObtenerDocumentoDWService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW", "MAR_T_PX_ObtenerDocumentoDWService");
    public final static QName ObtenerDocumentosDWM = new QName("http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW", "obtenerDocumentosDWM");
    static {
        URL url = MARTPXObtenerDocumentoDWService.class.getResource("/META-INF/wsdl/obtenerDocumentosADR.wsdl");
        if (url == null) {
            url = MARTPXObtenerDocumentoDWService.class.getClassLoader().getResource("/META-INF/wsdl/obtenerDocumentosADR.wsdl");
        } 
        if (url == null) {
            java.util.logging.Logger.getLogger(MARTPXObtenerDocumentoDWService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "/META-INF/wsdl/obtenerDocumentosADR.wsdl");
        }       
        WSDL_LOCATION = url;
    }

    public MARTPXObtenerDocumentoDWService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public MARTPXObtenerDocumentoDWService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public MARTPXObtenerDocumentoDWService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public MARTPXObtenerDocumentoDWService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public MARTPXObtenerDocumentoDWService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public MARTPXObtenerDocumentoDWService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns ObtieneDocumentosPortType
     */
    @WebEndpoint(name = "obtenerDocumentosDWM")
    public ObtieneDocumentosPortType getObtenerDocumentosDWM() {
        return super.getPort(ObtenerDocumentosDWM, ObtieneDocumentosPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ObtieneDocumentosPortType
     */
    @WebEndpoint(name = "obtenerDocumentosDWM")
    public ObtieneDocumentosPortType getObtenerDocumentosDWM(WebServiceFeature... features) {
        return super.getPort(ObtenerDocumentosDWM, ObtieneDocumentosPortType.class, features);
    }

}
