package com.esa.marketsales.contractmgnt.t.obtenerdocumentodw;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.BindingProvider;

/**
 * Created by Daniel Arias on 18/02/2019.
 */
public class clienteDocumentosADR {

    /**
     * connect timeout for weblogic server.
     */
    private static final String WLS_CONNECT_TIMEOUT = "com.sun.xml.internal.ws.connect.timeout";
    /**
     * read timeout for weblogic server.
     */

    private static final String WLS_READ_TIMEOUT = "com.sun.xml.internal.ws.request.timeout";

    private static final String END_POINT = "javax.xml.ws.service.endpoint.address";

    private static final String HTTP_HEADER = "javax.xml.ws.http.request.headers";

    private clienteDocumentosADR() {
    }

    public static ObtieneDocumentosSalidaType obtieneDocumentosRequest(
            String url,
            int rTimeout,
            int cTimeout,
            ObtieneDocumentosEntradaType requestREQType
    ) throws ObtieneDocumentosFaultMsg {
        MARTPXObtenerDocumentoDWService ss = new MARTPXObtenerDocumentoDWService();
        ObtieneDocumentosPortType port = ss.getObtenerDocumentosDWM();

        Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();

        requestContext.put(WLS_READ_TIMEOUT, rTimeout);
        requestContext.put(WLS_CONNECT_TIMEOUT, cTimeout);
        requestContext.put(END_POINT, url);

        return port.obtenerDocumentosDWM(requestREQType);
    }

}
