
package com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.type;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.ErrorType;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.ObtieneDocumentosEntradaType;
import com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.ObtieneDocumentosSalidaType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.type package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObtieneDocumentosResponse_QNAME = new QName("http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW/type", "ObtieneDocumentosResponse");
    private final static QName _ObtieneDocumentosFault_QNAME = new QName("http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW/type", "ObtieneDocumentosFault");
    private final static QName _ObtieneDocumentosRequest_QNAME = new QName("http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW/type", "ObtieneDocumentosRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.esa.marketsales.contractmgnt.t.obtenerdocumentodw.type
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtieneDocumentosSalidaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW/type", name = "ObtieneDocumentosResponse")
    public JAXBElement<ObtieneDocumentosSalidaType> createObtieneDocumentosResponse(ObtieneDocumentosSalidaType value) {
        return new JAXBElement<ObtieneDocumentosSalidaType>(_ObtieneDocumentosResponse_QNAME, ObtieneDocumentosSalidaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErrorType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW/type", name = "ObtieneDocumentosFault")
    public JAXBElement<ErrorType> createObtieneDocumentosFault(ErrorType value) {
        return new JAXBElement<ErrorType>(_ObtieneDocumentosFault_QNAME, ErrorType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtieneDocumentosEntradaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.esa.com/MarketSales/ContractMgnt/T/ObtenerDocumentoDW/type", name = "ObtieneDocumentosRequest")
    public JAXBElement<ObtieneDocumentosEntradaType> createObtieneDocumentosRequest(ObtieneDocumentosEntradaType value) {
        return new JAXBElement<ObtieneDocumentosEntradaType>(_ObtieneDocumentosRequest_QNAME, ObtieneDocumentosEntradaType.class, null, value);
    }

}
